namespace Surv

module Generator =
    let tryGenerate (random:System.Random) (generator:Map<'T,int>) : 'T option =
        let total = generator |> Map.fold (fun a _ v->a+v) 0
        let generated = random.Next(total)
        generator
        |> Map.fold 
            (fun (r,g) k v -> 
                match r with
                | Some x -> (Some x, 0)
                | None ->
                    if g < v then
                        (Some k, 0)
                    else
                        (None, (g-v))) (None, generated)
        |> fst

