namespace Surv

type World =
    {
        random: System.Random
        avatar: Avatar
        areas: Map<Location, Area>
    }

module World =
    let private setArea (location:Location) (area:Area) (world:World) : World =
        {world with areas=world.areas |> Map.add location area}

    let private generateAvatarArea (world:World) : World =
        let avatarLocation = world.avatar.location
        if world.areas.ContainsKey (avatarLocation) then
            world
        else
            world
            |> setArea avatarLocation (Area.create world.random)

    let create () : World =
        let random = System.Random()
        {
            random = random
            avatar = Avatar.create random
            areas = Map.empty
        }
        |> generateAvatarArea

    let private transformAvatar (transform:Avatar->Avatar) (world:World) : World =
        {world with avatar = world.avatar |> transform}

    let turnAvatar (direction:TurnDirection) (world:World) : World =
        world
        |> transformAvatar (Avatar.turn direction)
        |> generateAvatarArea

    let moveAvatar (direction:MoveDirection) (world:World) : World =
        world
        |> transformAvatar (Avatar.move direction)
        |> generateAvatarArea

    let private tryGetArea (location:Location) (world:World) : Area option =
        world.areas
        |> Map.tryFind location

    let getArea (location:Location) = 
        tryGetArea location >> Option.get

    let getAvatarArea (world:World) : Area =
        world
        |> getArea world.avatar.location

    let setAvatarHome (world:World) : World =
        world
        |> transformAvatar (Avatar.setHome (Some world.avatar.location))

    let clearAvatarHome (world:World) : World =
        world
        |> transformAvatar (Avatar.setHome None)


