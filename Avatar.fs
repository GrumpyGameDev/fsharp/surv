namespace Surv

type Avatar =
    {
        facing: Direction
        location: Location
        home: Location option
    }

module Avatar =
    let create (random:System.Random) : Avatar =
        {
            facing = [ North; South; East; West ] |> List.sortBy (fun _ -> random.Next()) |> List.head
            location = { x = random.Next(); y = random.Next() }
            home = None
        }

    let setHome (home: Location option) (avatar:Avatar) : Avatar =
        {avatar with home=home}

    let turn (direction:TurnDirection) (avatar:Avatar) : Avatar =
        let newFacing = 
            match direction, avatar.facing with
            | TurnDirection.Right, North | TurnDirection.Left, South | Around, West  -> East
            | TurnDirection.Right, East  | TurnDirection.Left, West  | Around, North -> South
            | TurnDirection.Right, South | TurnDirection.Left, North | Around, East  -> West
            | TurnDirection.Right, West  | TurnDirection.Left, East  | Around, South -> North
        {avatar with facing = newFacing}

    let move (moveDirection:MoveDirection) (avatar:Avatar) : Avatar =
        let direction =
            match moveDirection, avatar.facing with
            | Ahead, North | Right, West  | Left, East  | Back, South -> North
            | Ahead, East  | Right, North | Left, South | Back, West  -> East
            | Ahead, South | Right, East  | Left, West  | Back, North -> South
            | Ahead, West  | Right, South | Left, North | Back, East  -> West
        let deltaX, deltaY =
            match direction with
            | North -> (0,1)    
            | East -> (1,0)
            | South -> (0,-1)
            | West -> (-1,0)
        {avatar with location = avatar.location |> Location.setX (avatar.location.x+deltaX) |> Location.setY (avatar.location.y+deltaY)}
