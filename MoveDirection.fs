namespace Surv

type MoveDirection = 
    | Ahead
    | Left
    | Right
    | Back

