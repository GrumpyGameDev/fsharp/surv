﻿open Surv

[<EntryPoint>]
let main argv =
    World.create()
    |> Runner.run
    0
