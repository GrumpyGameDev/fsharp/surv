namespace Surv

type Location =
    {
        x: int
        y: int
    }

module Location =
    let setX (x:int) (location:Location) : Location =
        {location with x = x}

    let setY (y:int) (location:Location) : Location =
        {location with y = y}

    let delta (from:Location) (``to``:Location) : Location =
        {
            x = ``to``.x-from.x
            y = ``to``.y-from.y
        }

    let magnitude (location:Location) : float =
        sqrt((float)location.x*(float)location.x+(float)location.y*(float)location.y)

    let distance  (from:Location) (``to``:Location) : float=
        delta from ``to``
        |> magnitude

    let private turnClockwise (location:Location) : Location =
        {
            x = location.y
            y = -location.x
        }

    let private turnCounterclockwise = turnClockwise >> turnClockwise >> turnClockwise

    let toFacing (facing:Direction) (location:Location): Location =
        match facing with
        | North -> location
        | East -> location |> turnCounterclockwise
        | South -> location |> turnCounterclockwise |> turnCounterclockwise
        | West -> location |> turnCounterclockwise |> turnCounterclockwise |> turnCounterclockwise



