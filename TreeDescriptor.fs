namespace Surv

type TreeDescriptor =
    {
        woodLeft: int
    }

module TreeDescriptor=
    let create (random:System.Random) : TreeDescriptor =
        {
            woodLeft = random.Next(5,11)+random.Next(0,6)
        }