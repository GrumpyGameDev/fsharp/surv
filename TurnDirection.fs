namespace Surv

type TurnDirection =
    | Left
    | Right
    | Around

