namespace Surv

type Area =
    {
        terrain: Terrain
    }

module Area =
    let create (random:System.Random) : Area =
        {
            terrain = random |> Terrain.create
        }