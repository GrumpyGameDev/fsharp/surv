namespace Surv

type Direction =
    | North
    | East
    | South
    | West
