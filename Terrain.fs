namespace Surv

type Terrain =
    | Grass
    | Tree of TreeDescriptor
    | Stump

module Terrain =
    let private table = 
        [
            (Grass,5)
            (Tree {woodLeft=0}, 1)
        ] |> Map.ofList

    let create (random:System.Random) : Terrain =
        match table |> Generator.tryGenerate random with
        | Some (Tree _) ->
            TreeDescriptor.create random
            |> Tree
        | x -> 
            x 
            |> Option.get



