namespace Surv

module Runner = 
    let rec private confirm (prompt:string) : bool =
        prompt
        |> printfn "\n%s\n[Y]es\n[N]o"
        match System.Console.ReadKey(true).Key with
        | System.ConsoleKey.Y -> true
        | System.ConsoleKey.N -> false
        | _ -> confirm prompt

    let rec private handleTurn (world:World) : World =
        printfn "\nTurn how?\n[L]eft\n[R]ight\n[A]round\n[C]ancel"
        match System.Console.ReadKey(true).Key with
        | System.ConsoleKey.L ->
            printfn "\nYou turn left."
            world |> World.turnAvatar TurnDirection.Left
        | System.ConsoleKey.R ->
            printfn "\nYou turn right."
            world |> World.turnAvatar TurnDirection.Right
        | System.ConsoleKey.A ->
            printfn "\nYou turn around."
            world |> World.turnAvatar Around
        | System.ConsoleKey.C ->
            world
        | _ -> handleTurn world

    let rec private handleMove (world:World) : World =
        printfn "\nMove how?\n[A]head\n[L]eft\n[R]ight\n[B]ack\n[C]ancel"
        match System.Console.ReadKey(true).Key with
        | System.ConsoleKey.L ->
            printfn "\nYou move left."
            world |> World.moveAvatar MoveDirection.Left
        | System.ConsoleKey.R ->
            printfn "\nYou move right."
            world |> World.moveAvatar MoveDirection.Right
        | System.ConsoleKey.A ->
            printfn "\nYou move ahead."
            world |> World.moveAvatar Ahead
        | System.ConsoleKey.B ->
            printfn "\nYou move back."
            world |> World.moveAvatar Back
        | System.ConsoleKey.C ->
            world
        | _ -> handleMove world

    let rec private handleHome (world:World) : World =
        match world.avatar.home with
        | Some l ->
            let delta = Location.delta world.avatar.location l |> Location.toFacing world.avatar.facing
            (delta.x,delta.y) ||> printfn "(%d,%d)"
            match delta.x with
            | 0 ->
                match delta.y with
                | 0 ->
                    printfn "\nYou are at home."
                | y when y>0 ->
                    printfn "\nHome is directly ahead of you."
                | _ ->
                    printfn "\nHome is directly behind you."
            | x when x>0 ->
                match delta.y with
                | 0 ->
                    printfn "\nHome is directly to your right."
                | y when y>0 ->
                    printfn "\nHome is ahead and to your right."
                | _ ->
                    printfn "\nHome is behind you and to your right."
            | _ ->
                match delta.y with
                | 0 ->
                    printfn "\nHome is directly to your left."
                | y when y>0 ->
                    printfn "\nHome is ahead and to your left."
                | _ ->
                    printfn "\nHome is behind you and to your left."
        | None ->
            printfn "\nYou do not have a home set."
        printfn "\n[S]et to current position\n[U]nset/clear\n[C]ancel"
        match System.Console.ReadKey(true).Key with
        | System.ConsoleKey.S ->
            if confirm "Are you sure you want to set your current position as home?" then
                printfn "\nYou set home to current position."
                world
                |> World.setAvatarHome
            else
                world
        | System.ConsoleKey.U ->
            if confirm "Are you sure you want to clear your home position?" then
                printfn "\nYou unset/cleared your home position."
                world
                |> World.clearAvatarHome
            else
                world
        | System.ConsoleKey.C ->
            world
        | _ -> handleHome world

    let rec run (world:World) : unit =
        let area = world |> World.getAvatarArea
        (area.terrain.ToString()) |> printfn "Terrain: %s"

        printfn "[T]urn\n[M]ove\n[H]ome\n[Q]uit"
        match System.Console.ReadKey(true).Key with
        | System.ConsoleKey.H ->
            world
            |> handleHome
            |> run


        | System.ConsoleKey.T ->
            world 
            |> handleTurn 
            |> run

        | System.ConsoleKey.M ->
            world 
            |> handleMove 
            |> run

        | System.ConsoleKey.Q ->
            if confirm "Are you sure you want to quit?" then
                ()
            else
                run world
        | _ ->
            run world

